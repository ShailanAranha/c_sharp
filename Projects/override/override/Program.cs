﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @override
{
    // The ability of a subclass to override a method allows 
    //a class to inherit from a superclass whose behavior is "close enough"
    //and then to modify behavior as needed. 
    //Overriding is a feature that is available while using Inheritance.
    // same method name but exhibit different behaviour and can be modified as per the need


    class baseclass
    {
        public virtual void method()
        {
            Console.WriteLine("Hi! baseclass");
            Console.ReadLine();
        }
    }

    class derivedclass : baseclass
    {
        public override void method()
        {
            Console.WriteLine("Hi! subclass");
           // base.method();
            Console.ReadLine();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            baseclass b = new derivedclass();
                b.method();

            /*
            int[] myarray = new int[] { 4,5,7,9,3,5,8 };
            Array.Sort(myarray);
            foreach (int item in myarray)
            {
                Console.WriteLine(item);

                
            }

            Console.ReadLine();
            */
        }
    }
}
