﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collections_practise
{
    class Program
    {
        static void Main(string[] args)
        {
            doctor.doctorwho();
            doctor doctor11 = new doctor();
            doctor11.name = "matt smith";
            doctor11.id = "tardis";

            doctor doctor10 = new doctor();
            doctor10.name = "david tanent";
            doctor10.id = "tardis";

            companion companion1 = new companion("amy", 1);
            companion companion2 = new companion("river", 2);
            companion companion3 = new companion("rose", 3);

            /*
            ArrayList myarraylist = new ArrayList();
            myarraylist.Add(doctor11);
            myarraylist.Add(doctor10);
         //  myarraylist.Add(companion1);

            foreach (doctor a in myarraylist)
            {
                Console.WriteLine(a.name);
            }
            Console.ReadLine();
            */

            List<doctor> mylist = new List<doctor>();
            mylist.Add(doctor10);
            mylist.Add(doctor11);
            foreach (doctor a in mylist)
            {
                Console.WriteLine(a.name);
            }
            Console.ReadLine();

            //var mj = from doc in mylist where doc.id == "tardis" select doc;

            var mj = mylist.Where(p => p.id == "tardis");
            


            Console.WriteLine(mj.Count());
            foreach (var MJ in mj)
            {
                Console.WriteLine(MJ.name);
            }
            Console.ReadLine();

        }
    }

    class doctor
    {
        public string name { get; set; }
        public string id { get; set; }

        public static void doctorwho()
        { Console.WriteLine("doctor who"); }
    }

    class companion
    {
        public string name { get; set; }
        public int id { get; set; }

        public companion(string Name, int ID)
        {
            name = Name;
            id = ID;
        }
    }
}
