﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_classesAND_interfaces
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Car car = new Car();
            var truck = new truck();
           // car.start();
           // car.stop();
            //car.timetable();

            Operation(car);
            Operation(truck);



        }

        static void Operation(IMachine machine)
        {
            machine.start();
        }
    }
   

    public interface IMachine
    {
         void start();
         void stop();
    }
     
   

    public interface IRoutine
    {
        void timetable();
    }
    class Car:IMachine, IRoutine
    {
        public void start()
        {
            Console.WriteLine("car starts");
            Console.ReadLine();
        }

        public void stop()
        {
            Console.WriteLine("car stops");
            Console.ReadLine();
        }

        public void timetable()
        {
            Console.WriteLine("car schedule");
            Console.ReadLine();
        }

    }

    class truck: IMachine
    {
        public void start()
        {
            Console.WriteLine("truck starts");
            Console.ReadLine();
        }

        public void stop()
        {
            Console.WriteLine("truck stops");
            Console.ReadLine();
        }
    }

    
}
