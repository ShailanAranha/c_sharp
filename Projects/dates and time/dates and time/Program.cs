﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dates_and_time
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("{0:C}",1234);
            Console.ReadLine()
            //DateTime myvalue = DateTime.Now;
            // Console.WriteLine(myvalue.ToLocalTime());
            // Console.WriteLine(myvalue.AddDays(-3).ToLocalTime());
            // Console.WriteLine(myvalue.AddMonths(3).ToLocalTime());

            // DateTime mybirthday = new DateTime(1996, 08, 16);
            // Console.WriteLine(mybirthday.TimeOfDay);
            DateTime mybirthday = DateTime.Parse("16/08/1996");
            TimeSpan myage = DateTime.Now.Subtract(mybirthday);
            Console.WriteLine(myage.TotalDays);                

            Console.ReadLine();
        }
    }
}
