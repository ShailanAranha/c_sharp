﻿using mylib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practise4
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Test mytest = new Test();
            string value =mytest.ScrapeWebpage("https://www.justinguitar.com/");
            Console.WriteLine(value);
            Console.ReadLine();
           */

            Car mycar1 = new Car("India", "nissan", 3810);
            Car mycar2 = new Car("china", "nano", 3811);

            Book mybook = new Book();
            mybook.Name = "gaurdians of th galaxy";
            mybook.Number = 5656;

            //ArrayList
            /*
            ArrayList myarrylist = new ArrayList();
            myarrylist.Add(mycar1);
            myarrylist.Add(mycar2);

            foreach (Car car in myarrylist)
            {
                Console.WriteLine(car.Make);
            }
            */

            // lists<>
            /*
            List<Car> mylist = new List<Car>();
            mylist.Add(mycar1);
            mylist.Add(mycar2);

            foreach (Car car in mylist)
            {
                Console.WriteLine(car.Model);
            }
            */
            /*
            //dictionary
            Dictionary<int, Car> mycar = new Dictionary<int, Car>();
            mycar.Add(mycar1.Number, mycar1);
            mycar.Add(mycar2.Number, mycar2);

            Console.WriteLine(mycar[3810].Model);
            */

            //object initializer
            Car mycar3 = new Car() { Make="japan", Model="honda", Number=3812 };

            //collection initializer
            
            
            Console.ReadLine();
             
        }

        class Car
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public int Number { get; set; }

            static Car()
            {
                Console.WriteLine("welcome to cars");
            }

            public Car(string make, string model, int number)
            {
                this.Make = make;
                this.Model = model;
                this.Number = number;
            }

            public Car()
            {
            }
        }

        class Book
        {
            public string Name { get; set; }
            public int Number { get; set; }

            static Book()
            {
                Console.WriteLine("BOOKS!!");
            }

            
        }

    }
}
