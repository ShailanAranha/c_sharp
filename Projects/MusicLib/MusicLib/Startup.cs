﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MusicLib.Startup))]
namespace MusicLib
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
