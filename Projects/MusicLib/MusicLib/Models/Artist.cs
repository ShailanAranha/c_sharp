﻿using System.Collections.Generic;

namespace MusicLib.Models
{
    public class Artist
    {
        public int ArtistID { get; set; }
        public string Name { get; set; }
        public List<Album> Albums { get; set; }
    }
}