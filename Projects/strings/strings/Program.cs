﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strings
{
    class Program
    {
        static void Main(string[] args)
        {
            //string mystring = "hey \"shailan\"";
            //string mystring = @"open c:\\drive";
            // string mystring = string.Format("{0:C}", 1254);
            //string mystring = string.Format("{0:N}", 1254654000);
            //string mystring = string.Format("{0:P}", .124);
            //string mystring = string.Format("+91-{0:##########}",1234567890);
            string mystring = "lights will guide u home";
            //mystring = mystring.Substring(12);
            mystring = mystring.ToUpper();
            mystring = mystring.Replace(" ", "__");
            
            Console.WriteLine(mystring);
            Console.ReadLine();
        }
    }
}
