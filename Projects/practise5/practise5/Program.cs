﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace practise5
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            try
            {
                string content = File.ReadAllText(@"C:\Users\Shailan Aranha\Desktop\blue.txt");
                Console.WriteLine(content);
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Opps!! There is a problem");
                Console.WriteLine(e.Message);
                //Console.WriteLine(e.InnerException);
                Console.ReadLine();
                
            }
            */

            //event handling

            Timer mytimer = new Timer(2000);
            mytimer.Elapsed += Mytimer_Elapsed;
            mytimer.Elapsed += Mytimer_Elapsed1;
            mytimer.Start();
            Console.WriteLine("press enter key to remove the time1");
            Console.ReadLine();
            mytimer.Elapsed -= Mytimer_Elapsed1;

            Console.ReadLine();
        }

        private static void Mytimer_Elapsed1(object sender, ElapsedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Time1: {0:hh:mm:ss.fff}", e.SignalTime);
        }

        private static void Mytimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Time2: {0:hh:mm:ss.fff}",e.SignalTime);
        }
    }
}
