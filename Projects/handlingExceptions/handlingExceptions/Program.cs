﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace handlingExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string content = File.ReadAllText(@"C:\Users\Shailan Aranha\Desktop\c#\downloads\cs-code\Lesson_22\exception.txtt");
                Console.WriteLine(content);



            }

            catch (FileNotFoundException ex)
            {
                Console.WriteLine("problem with the file name");
                Console.WriteLine(ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine("problem with the directory name");
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("problem");
                Console.WriteLine(ex.Message);


            }
            finally
            {
                Console.WriteLine("closing the application.....");
            }
           
            Console.ReadLine();


        }
    }
}
