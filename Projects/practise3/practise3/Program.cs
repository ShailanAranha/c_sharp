﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practise3
{
    class Program
    {
        static void Main(string[] args)
        {
           
            
            
             Car.myMethod();
             Console.ReadLine();
           
             Car myCar = new Car();
             Console.WriteLine("{0} {1}", myCar.Make, myCar.Model);
             Console.ReadLine();

             Car mythirdCar = new Car("ASD", 2016, "qwe", "red" );
             Console.WriteLine("{0} {1}", mythirdCar.Make, mythirdCar.Model);
             Console.ReadLine();

             myCar.Make = "india";
             myCar.Model = "BMW";
             Console.WriteLine("{0} {1}", myCar.Make, myCar.Model);
             Console.ReadLine();


             Car myOthercar = myCar;
             myOthercar.Make = "Mumbai";
             myOthercar.Model = "Audi";

             Console.WriteLine("{0} {1}", myCar.Make, myCar.Model);
             Console.ReadLine();

             myOthercar = null;
             


        }
    }

    class Car
    {
        public string Make { get; set; }
        public int Year { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }

        public Car()
        {
            this.Make = "nissan";
            this.Year = 2017;
            this.Model = "n25";
        }

        public Car(string make, int year, string model, string colour)
        {
            this.Make = make;
            this.Model = model;
            this.Year = year;
            this.Colour = colour;
        }
         static Car()
        {
            Console.WriteLine("Welcocme to the Car Exibition");
        }
        

        public static void myMethod()
        {
            Console.WriteLine("my name is shailan aranha");
        }
    }
}

