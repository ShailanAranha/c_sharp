﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hello_world_ASP.Models
{
    public class Artist
    {
        public int ArtistID { get; set; }
        public string Name { get; set; }
        public List<Album> Albums { get; set; }
    }
}