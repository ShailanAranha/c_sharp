﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(hello_world_ASP.Startup))]
namespace hello_world_ASP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
