﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practise2
{
    class Program
    {
        static void Main(string[] args)
        {
            //understanding classes
            
            Car myCar = new Car();
            myCar.Make = "BMW";
            myCar.Model = 3810;
            myCar.Colour = "red";
            myCar.Year = 2016;

            Console.WriteLine("{0} {1} {2} {3}", myCar.Make, myCar.Model, myCar.Colour, myCar.Year);
            Console.ReadLine();

            decimal value = carPrice(myCar);
            Console.WriteLine("the price of car is {0}", value);
            Console.ReadLine();

            Console.WriteLine("the price of car is {0:P}", myCar.CarDecimalValue());
            Console.ReadLine();
        }

        private static decimal carPrice(Car car)
        {
            decimal carValue = 100.00M;
            return carValue;
        }
    }

    class Car
    {
        public string Make { get; set; }
        public int Model { get; set; }
        public string Colour { get; set; }
        public int Year { get; set; }
        
        public decimal CarDecimalValue()
        {
            decimal carValue;
            if (Year > 1996)
                carValue = 10000;
            else
                carValue = 2000;

            return carValue;
        }
    }
}
