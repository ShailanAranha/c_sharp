﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Car> myCars = new List<Car>() {
                new Car() { VIN="A1", Make = "BMW", Model= "550i", StickerPrice=55000, Year=2009},
                new Car() { VIN="B2", Make="Toyota", Model="4Runner", StickerPrice=35000, Year=2010},
                new Car() { VIN="C3", Make="BMW", Model = "745li", StickerPrice=75000, Year=2008},
                new Car() { VIN="D4", Make="Ford", Model="Escape", StickerPrice=25000, Year=2008},
                new Car() { VIN="E5", Make="BMW", Model="55i", StickerPrice=57000, Year=2010}
            };

            List<Car> myanothercar = new List<Car>() {
                new Car() { VIN="A1", Make = "BMW", Model= "550i", StickerPrice=55000, Year=2009},
                new Car() { VIN="B2", Make="Toyota", Model="4Runner", StickerPrice=35000, Year=2010},
                new Car() { VIN="C3", Make="BMW", Model = "745li", StickerPrice=75000, Year=2008},
                new Car() { VIN="D4", Make="Ford", Model="Escape", StickerPrice=25000, Year=2008},
                new Car() { VIN="E5", Make="BMW", Model="55i", StickerPrice=57000, Year=2010}
            };

            //LINQ query
            /*
            var bmws = from car in myCars where car.Make == "BMW" && car.Year==2010 select car;
            foreach (var car in bmws)
            {
                Console.WriteLine(car.Model);
                Console.ReadLine();
            }
            */


            //linq method
            /*
            var bmws = myCars.Where(p => p.Make == "BMW" && p.Year == 2010);

            foreach (var car in bmws)
            {
                Console.WriteLine(car.Model);
                Console.ReadLine();
            }
            */

            //linq query
           // var bmws = from car in myCars where car.Model == (from car1 in myanothercar where car.Make=="bmw" select car1.Model) select car;
            var bmw = from car in myCars where car.Make == "Ford" || car.Model== "550i" select car;
            foreach (var car in bmw)
            {
                Console.WriteLine(car.Model);
                Console.ReadLine();
            }

        }
    }

    class Car
    {
        public string VIN { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public double StickerPrice { get; set; }
    }
}
