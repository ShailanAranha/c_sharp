﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dog1
{
    public class triner
    {
        void operate()
        {
        }
    }

     public partial class dog
    {

        public string name { get; set; }

        private string _name;

        public string Name
        {
            get { return _name; }
            private set { _name = value; }
        }

        public event EventHandler hasSpoken;


        public void speak(string what= "bark")
        {
            if (hasSpoken != null)
                hasSpoken(this, EventArgs.Empty);

    
        }

        public void speak(int times , string what = "bark")
        {

        }

        // only within class
        private void foo()
        { }

        //only for derived and this class
        protected void Bar()
        { }

        //only in ths assembly
        internal void Ba()
        { }
    }
    class pug : dog
    {
        void features()
        {
            this.speak(2, "woof");
        }
    }

}
