﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[10];
            Console.Write("Enter the number of elements for sortning: ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the elements for sortning: ");
            for(int i=0; i < n; i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            
            for(int p=0; p<n; p++)
            {
                for(int j=0; j<n-1; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        int temp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = temp;
                    }
                }
            }
            Console.Write("the sorted elements are: ");

            foreach (int a in array)
            {
                Console.Write(a+" ");
            }
            Console.ReadLine();
        }
    }
}
