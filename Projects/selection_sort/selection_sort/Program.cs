﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace selection_sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myarray = new int[10];
            Console.Write("Enter the number of elements to be sorted:");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the elements:");
            for(int i=0; i < n; i++)
            {
                myarray[i] = int.Parse(Console.ReadLine());
            }

            method(myarray, n);
        }

        private static void method(int[] myarray, int n)
        {
           for(int i=0; i<n-1; i++)
            {
                int min_term = i;
                for(int j=i+1; j<n; j++)
                {
                    if (myarray[j] < myarray[min_term])
                    {
                        
                        min_term = j;

                    }
                  
                }
                // swap
                int temp = myarray[min_term];
                myarray[min_term] = myarray[i];
                myarray[i] = temp;
            }
            Console.Write("the sorted array is: ");
            for (int k = 0; k < n; k++)
            {
                Console.Write(myarray[k]+" ");

            }
            Console.ReadLine();
        }

       
    }
}
