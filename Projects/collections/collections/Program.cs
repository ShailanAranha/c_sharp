﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collections
{
    class Program
    {
        static void Main(string[] args)
        {
            student mystudent = new student();
            mystudent.Name = "shailan";
            mystudent.Id = 3810;

            student s1 = new student();
            s1.Name = "ashley";
            s1.Id = 3811;        

            book b1 = new book();
            b1.Auther = "mj";
            b1.grade = 8;

            // array sized dynamically
            // has some cool features
            /*
            ArrayList myarray = new ArrayList();

            myarray.Add(mystudent);
            myarray.Add(s1);
            myarray.Add(b1);
            myarray.Remove(b1);
            foreach (student student in myarray)
            {
                Console.WriteLine(student.Name);
                Console.ReadLine();

            }
            */


            // generic data type
            // allows us to work with specific data type
            /* List<student> mylist = new List<student>();

             mylist.Add(mystudent);
             mylist.Add(s1);

             foreach (student student in mylist)
             {
                 Console.WriteLine(student.Name);

             }
             Console.ReadLine();
         */

            /*   Dictionary<int, student> mydictionary = new Dictionary<int, student>();
               mydictionary.Add(mystudent.Id, mystudent);
               mydictionary.Add(s1.Id, s1);

               Console.WriteLine(mydictionary[3810].Name);
               Console.ReadLine();
               */



            //object instantization
            //no need of constructor
            //  student s2 = new student() { Name = "sagar", Id = 3773 };


            //collection initialization

            List<student> list = new List<student>()
            {
                new student {Name="bernard",Id=3674 },
                new student {Name="sagar", Id=3774 }
            };



            // var bmws = (from student in list select student.Id).Sum();
           // Console.WriteLine(bmws.Sum());
            
            var bmws = from student in list select student.Id;
            Console.WriteLine(bmws.Sum());



            Console.ReadLine();

            foreach (student student in list)
            {
                Console.WriteLine(student.Name);
                Console.ReadLine();

            }
            

        }

        class student
        {
            public string Name {get; set;}
            public int Id { get; set; }
            
        }    

        class book
        {
            public string Auther { get; set; }
            public int grade { get; set; }
        }
    }
}

