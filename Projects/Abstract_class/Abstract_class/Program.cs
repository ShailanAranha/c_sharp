﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_class
{
    class Program
    {
        static void Main(string[] args)
        {
            var car = new Car();
            car.start();
            car.stop();
        }
    }
    abstract class truck
    {
        public void timetable()
        {
            Console.WriteLine("machine schedule");
            Console.ReadLine();

        }
    }

    abstract class Machine
    {
       public void start()
        {
            Console.WriteLine("machine starts");
            Console.ReadLine();
        }
        
    }

    class Car: Machine
    {
        public void stop()
        {
            Console.WriteLine("machine stops");
            Console.ReadLine();
        }
    }
}

