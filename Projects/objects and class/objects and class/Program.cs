﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace objects_and_class
{
    class Program
    {
        static void Main(string[] args)
        {
            student mystudent = new student();

            student.mymethod();

            student studentx = new student("bernard", "comps", 3811, 10);
            Console.WriteLine(studentx.name);
            Console.ReadLine();
           /* mystudent.name = "shailan";
            mystudent.course = "comps";
            mystudent.Id = 3810;
            mystudent.grade = 5;
            */
            Console.WriteLine(String.Format("{0} {1} {2} {3}", mystudent.name,
                mystudent.course, mystudent.Id, mystudent.grade));

           Console.ReadLine();

            student anotherstudent;
            anotherstudent = mystudent;

            anotherstudent.name = "ashley";
            

            Console.WriteLine(String.Format("{0} {1} {2} {3}", anotherstudent.name,
               anotherstudent.course, anotherstudent.Id, anotherstudent.grade));

            Console.ReadLine();








        }

       
    }

    class student
    {
        public string name { get; set; }
        public string course { get; set; }
        public int Id { get; set; }
        public int grade { get; set; }


        public student()
        {
            this.course = "IT";
        }

        public student(string Name, string Course, int id, int Grade)
        {
            name = Name;
            course = Course;
            id = Id;
            grade = Grade;
        
        }

        public static void mymethod()
        {
            Console.WriteLine("this method id static");
            Console.ReadLine();
        }
    }

    
}

