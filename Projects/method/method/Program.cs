﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace method
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("what is your name: ");
            string name = Console.ReadLine();

            Console.WriteLine("which college:");
            string college = Console.ReadLine();

            Console.WriteLine("which university:");
            string university = Console.ReadLine();

            string reversedName= ReverseString(name);
            string reversedcollege=ReverseString(college);
            string reverseduniversity=ReverseString(university);

            Console.Write(String.Format("{0} {1} {2}", reversedName, reversedcollege, reverseduniversity));
            Console.ReadLine();

        }

        private static string ReverseString(string message)
        {
            char[] messageArray = message.ToCharArray();
            Array.Reverse(messageArray);
            return String.Concat(messageArray);



           /* foreach (char  item in messageArray)
            {
                Console.Write(item);
            }
            Console.Write(" ");
            */
        }
    }
}
