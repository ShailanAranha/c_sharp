﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            // pattern problems
            /*
            for (int i=1; i < 8; i++)
            {
                for(int j=1; j < i; j++)
                {
                    Console.Write(j);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
            */

            /*
            for (int i = 1; i <= 8; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(i);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
            */
            /*
            for (int i = 8; i >= 1; i--)
            {
                for (int j = 8; j >= i; j--)
                {
                    Console.Write(j);
                }
                Console.WriteLine();
            }
            
            for(int i=1; i<= 8; i++)
            {
                for (int j=8; j>=i; j--)
                {
                    Console.Write(j);
                }
                Console.WriteLine();
            }

            Console.ReadLine();
            */

            for (int i=1; i<=5; i++)
            {
                
                
                int k = 0; 

                while(k<5)
                {
                    Console.Write(" ");
                    k++;
                }
              
                /*
                for (int j=2; j<=i; j++)
                {
                    Console.Write(i);
                }
                */

                for (int m = (i - 1); m >= 1; m--)
                {
                    Console.Write(m);
                } 

                Console.WriteLine();

            }
           
            Console.ReadLine();


            /*  program to remove duplicate values
            int[] array = new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
           // Console.WriteLine(array.Length);
            for (int i=0; i <= array.Length-1; i++)
            {
                for(int j=i+1; j <= array.Length-1; j++)
                {
                    if (array[i] == array[j])
                    {
                        int k = j;
                        while(k< array.Length-1)
                        {
                            array[k] = array[k + 1];
                            k++;
                        }
                        Array.Resize(ref array, array.Length - 1);
                        j--; 
                    }
                }
            }


             foreach(var arr in array)
            {
                Console.WriteLine(arr);
            }
            Console.ReadLine();
            */

            /*
            char[] arr1 = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'};
            char[] arr2 = new char[] {'j', 'k', 'l', 'm', 'n', 'o' };
            int k = 0;
            for (int i =5; i<=9; i++)
            {


                
                for (int j=0; j<=5; j++)
                {
                    int c = 0;
                    if (c == 0)
                    {
                        Console.WriteLine(arr1[j]);
                        c = 1;
                        k++;
                    }
                    else
                    {
                        Console.WriteLine(" ");
                        c = 0;
                    }
                  
                }
                
            }
            Console.ReadLine();
            */
        }
        
    }
}
