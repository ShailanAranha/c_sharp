﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs_itermediate
{
    class Program
    {
        static void Main(string[] args)
        {
          
        }

        partial class animal
        {
            //public 
            public void dog()
            {

            }
            //assembly
            internal void cat()
            { }
            //this and derived classes
            protected void rat()
            { }
        }

        class type : animal
        {
            void A()
            {
                this.cat();
            }
        }
       
    }
}
